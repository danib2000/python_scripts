def prims(graph):
  """Finds the minimum spanning tree of a weighted graph using Prim's algorithm.

  Args:
    graph: The weighted graph to find the minimum spanning tree of.

  Returns:
    The minimum spanning tree of the graph.
  """

  # Initialize the minimum spanning tree.
  mst = set()

  # Initialize the set of visited nodes.
  visited = set()

  # Initialize the set of candidate edges.
  candidate_edges = set()
  for node in graph:
    for neighbor in graph[node]:
      candidate_edges.add((node, neighbor, graph[node][neighbor]))

  # While there are still unvisited nodes:
  while len(visited) < len(graph):
    # Find the edge with the least weight that connects a visited node to an unvisited node.
    edge = min(candidate_edges, key=lambda edge: edge[2])

    # Add the edge to the minimum spanning tree.
    mst.add(edge)

    # Add the unvisited node to the set of visited nodes.
    visited.add(edge[1])

    # Remove the edge from the set of candidate edges.
    candidate_edges.remove(edge)

  return mst


def main():
    graph = {
  'A': {'B': 1, 'C': 2},
  'B': {'A': 1, 'C': 3, 'D': 4},
  'C': {'A': 2, 'B': 3, 'D': 5},
  'D': {'B': 4, 'C': 5}
}

    mst = prims(graph)

    print(mst)


main()
